import React from "react";
import "../components/Login.css";
import { FaUserCircle } from "react-icons/fa";
import { IoMdArrowDropdown } from "react-icons/io";

function Login() {
  return (
    <>
      <div className="header">
        <p className="nourishwithsim">
          {" "}
          nourish<span className="red">with</span>sim{" "}
        </p>
        <p className="NWS"> NWS - Admin Panel </p>
      </div>

      <div className="outer">
        <h2>Nourish with SIM</h2>
        <h3>Admin Login</h3>

        <form>
          <div className="username">
            <label className="mail">
              <input
                type="text"
                name="username"
                placeholder="Email id"
                required="required"
              ></input>
            </label>
          </div>

          <br />

          <div className="password">
            <label>
              <input
                type="text"
                name="password"
                placeholder="Password"
                required="required"
              ></input>
            </label>
          </div>

          <div className="forgot">
            <p>Forget Password ?</p>
          </div>

          <button type="submit" className="Login">
            Log in
          </button>
        </form>
      </div>
    </>
  );
}

export default Login;
