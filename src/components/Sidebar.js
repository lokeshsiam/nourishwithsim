import React from "react";
import "../components/Sidebar.css";
import { MdDashboard } from "react-icons/md";
import { FaUsers } from "react-icons/fa";
import { FaUser } from "react-icons/fa";
import { IoNutritionSharp } from "react-icons/io5";
import { BsCalendar2Check } from "react-icons/bs";
import { GiStrongMan } from "react-icons/gi";
import { CgGym } from "react-icons/cg";
import { GiOnTarget } from "react-icons/gi";
import { FaConciergeBell } from "react-icons/fa";
import { BsFillMenuButtonWideFill } from "react-icons/bs";
import { AiOutlineRight } from "react-icons/ai";


function Sidebar() {
  return (
    <div className="sidebar">
      <div className="top">
        <div className="bars">
          <MdDashboard />
          <h3 className="home">Dashboard</h3>
          <div className="icon">
            <AiOutlineRight />
          </div>
        </div>
        <div className="bar">
          <FaUsers />
          <h3 className="home">Manage Admin Users</h3>
        </div>

        <div className="bar">
          <FaUser />
          <h3 className="home">Manage Registered Users</h3>
        </div>

        <div className="bar">
          <IoNutritionSharp />
          <h3 className="home">Manage Nutritionist</h3>
        </div>

        <div className="bar">
          <BsCalendar2Check />
          <h3 className="home">Manage Programs</h3>
        </div>

        <div className="bar">
          <GiStrongMan />
          <h3 className="home">Manage Exersise</h3>
        </div>

        <div className="bar">
          <CgGym />
          <h3 className="home">Manage Workout</h3>
        </div>

        <div className="bar">
          <GiOnTarget />
          <h3 className="home">Challenges</h3>
        </div>

        <div className="bar">
          <FaConciergeBell />
          <h3 className="home">Manage Nutrition and Meal</h3>
        </div>

        <div className="bar">
          <BsFillMenuButtonWideFill />
          <h3 className="home">Manage Content</h3>
        </div>
      </div>
    </div>
  );
}

export default Sidebar;
